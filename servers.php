<?php
require_once 'include/_universal.php';
require_once 'include/GameQ.php';

/**
* -----------------------------
* 
* THE PASSWORD FOR ALL SERVERS IS
* 
*           zanziwar
* 
* -----------------------------
*/
$srvpass = "zanziwar";

/** Xfire Translation */
$xfire = array(
                "cod4" => "cod4mp",
                "cssource" => "css"
                );

$x = new universal('servers','',0);
if($toggle['servers']&&$x->is_secure()) {
    /** Display the header */
	$x->display_top();
    
    /** Echo heading */
	echo "<strong>servers</strong>:<br />";
	echo "<br />";
    
    /** Display the related admin link */
	$x->add_related_link('add/modify servers','admin_serverlist.php',2);
	$x->display_related_links(); 
    
    ?>
        <div class="general_box">
            TeamSpeak 3: ts.zanzi.lan<br />
            IRC: irc.zanzi.lan<br />
            FTP: ftp.zanzi.lan<br />
        </div>
        <br />
    <?php
    
    /** Get the servers from the database */
	$data = $dbc->database_query('SELECT servers.gameid, servers.id, servers.ipaddress, servers.queryport, 
                        games.querystr2, games.name AS game_name, games.thumbs_dir, games.short 
                        FROM servers LEFT JOIN games USING (gameid) WHERE servers.tourneyid=0 ORDER BY games.name');
  
    /** Create GameQ instance */
    $gq = new GameQ();
    $gq->setFilter('normalise');
    
    /** Loop the results */
	while ($row = $dbc->database_fetch_array($data)) {
        /** Only query a server when the query type has been set (the game's name) */
		if (!empty($row['querystr2'])) {
            /** Game name given, proceed */          
            
            /** Check if the port is empty */
			if(empty($row['queryport'])) {
                $ipaddress = $row['ipaddress'];
                /** Cut the IP address in half if the query port hasn't been set */
				$ipaddress = explode(":", $ipaddress);
                
                /** Check if we do have a port now */
                if(!empty($ipaddress[1]))
                {
                    /** Push the server into the query array with 3 arguments */
                    $servers[$row['id']] = array($row['querystr2'], $ipaddress[0], $ipaddress[1]);
                } else {
                    /** Push the server into the query array with only 2 arguments (default port will be picked by GameQ) */
                    $servers[$row['id']] = array($row['querystr2'], $ipaddress[0]);
                }
			} else {
                /** Just enter the server into GameQ */
                $servers[$row['id']] = array($row['querystr2'], $row['ipaddress'], $row['queryport']);
            }
        }
    }
    
    /** Send the servers to GameQ */
    $gq->addServers($servers);
    
    /** Request the data, and display it */
    try {
        $gqdata = $gq->requestData();
        
        /** Display the servers table */
        echo "<table class='srvtbl'>";
            echo "<th colspan='3'>ZanziLAN Gameservers</th>";
            foreach($gqdata as $srv => $cvar)
            {
                echo "<tr class='row2'>";
                    
                    echo "<td width='48'>";
                        echo "<img src='/img/logos/" . $cvar['gq_type'] . ".png' />";
                    echo "</td>";
                    
                    /** Make sure the server is online before displaying any information */
                    if($cvar['gq_online'] == "1") {
                        
                        /** Display short server stats */
                        echo "<td>";
                            echo "<a href='viewserver.php?ip=" . $cvar['gq_address'] . "&port=" . $cvar['gq_port'] . "&enginetype=" . $cvar['gq_type'] . "'>" . $cvar['gq_hostname'] . "</a><br />";
                            echo "<span class='sub'>" . $cvar['gq_numplayers'] . "/" . $cvar['gq_maxplayers'] . " - " . $cvar['gq_mapname'] . "</span>";
                        echo "</td>";
                        
                        /** Server Join Buttons */
                        echo "<td align='right'>";
                            if($cvar['gq_prot'] == "source") {
                                /** Password check */
                                if($cvar['gq_password'] == 1) {
                                    /** Passworded */
                                    echo "<a href='steam://connect/" . $cvar['gq_address'] . ":" . $cvar['gq_port'] . "/" . $srvpass . "'><img src='img/logos/steam.png' /></a> ";
                                } else {
                                    /** No password */
                                    echo "<a href='steam://connect/" . $cvar['gq_address'] . ":" . $cvar['gq_port'] . "'><img src='img/logos/steam.png' /></a> ";
                                }
                            }
                            
                            if($cvar['gq_password'] == 1) {
                                echo "<a href='xfire:join?game=" . $xfire[$cvar['gq_type']] . "&server=" . $cvar['gq_address'] . ":" . $cvar['gq_port'] . "&password=" . $srvpass . "'><img src='img/logos/xfire.png' /></a>";
                            } else {
                                /** No password */
                                echo "<a href='xfire:join?game=" . $xfire[$cvar['gq_type']] . "&server=" . $cvar['gq_address'] . ":" . $cvar['gq_port'] . "'><img src='img/logos/xfire.png' /></a>";
                            }
                            
                            echo "<a href='hlsw://" . $cvar['gq_address'] . ":" . $cvar['gq_port'] . "'><img src='img/logos/hlsw.png' /></a> ";
                        echo "</td>";
                    } else {
                        /** Server is offline, display less */
                        echo "<td>";
                            echo "<a href='viewserver.php?ip=" . $cvar['gq_address'] . "&port=" . $cvar['gq_port'] . "&enginetype=" . $cvar['gq_type'] . "'>Server Offline</a><br />";
                            echo "<span class='sub'>0/0 - None</span>";
                        echo "</td>";    
                        
                        echo "<td></td>";
                    }
                    
                echo "</tr>";
            }
        echo "</table>";        
    }                             
    /** Catch any errors that might have occurred */
    catch (GameQ_Exception $e) {
        echo 'an error occurred while fetching the server data.';
    }    
            
	$x->display_bottom();
} else {
	$x->display_slim('you are not authorized to view this page.');
}
?>