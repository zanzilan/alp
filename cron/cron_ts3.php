<?php if ( PHP_SAPI != "cli" ) exit('No direct script allowed');
	/**
	 * ALP TeamSpeak3 Cron Job
	 * 
	 */
	date_default_timezone_set("Europe/Brussels");
	
	$libpath='../include/'; // Set include path

	// Load TeamSpeak Server Config
//	require_once($libpath . "_config_ts3.php");
    require_once("../_config.php");

	// load framework library
	require_once($libpath . "TeamSpeak3/TeamSpeak3.php");
	
	/**
	 * TeamSpeak 3 Server Viewer
	 * Supposed to be simple...
	 */
	try
	{
		/* connect to server, authenticate and get TeamSpeak3_Node_Server object by URI */
		$ts3 = TeamSpeak3::factory("serverquery://" . $ts3["user"] . ":" . $ts3["pass"] . "@" . $ts3["host"] . ":" . $ts3["query"] . "/?server_port=" . $ts3["voice"] . "#no_query_clients");
	
		/* enable new display mode */
		$ts3->setLoadClientlistFirst(TRUE);
	
		/* display viewer for selected TeamSpeak3_Node_Server */
		$ts3View = $ts3->getViewer(new TeamSpeak3_Viewer_Html("/img/viewer/", "/img/flags/", "data:image"));

        // Open cache file for writing
        $ts3Cache = fopen($libpath."ServerCache/ts3.dat", 'w');
        if($ts3Cache == FALSE)
            throw new Exception("Error opening ts3.dat. Permissions correct?");

        // Write the data to the cache file
        fwrite($ts3Cache, $ts3View);
	}
	catch(Exception $e)
	{
		/* echo error message */
		echo "<p><span class=\"error\"><b>ERROR 0x" . dechex($e->getCode()) . "</b>: " . htmlspecialchars($e->getMessage()) . "</span></p>";
	}
	
	// Close the file pointer
	fclose($ts3Cache);
?>