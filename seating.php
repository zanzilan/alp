<?php
require_once 'include/_universal.php';
$x = new universal('seating chart','seating',0);

$x->display_top();

	$mysqli = new mysqli($database['server'], $database['map_user'], $database['map_passwd'], $database['map_database']);
	
	/* check connection */
	if ($mysqli->connect_errno) {
	    printf("Connect failed: %s\n", $mysqli->connect_error);
	    exit();
	}
	
	printf("<div class=\"seatcontainer\">");
	printf("<span class=\"celltitle\" style=\"padding-left: 5px; padding-right: 700px\">seats</span>");

    /**
     * Header Combo Box
     */
    printf("<div title=\"Not here, dumbass.. :)\" class=\"notification\"><strong>Hou je cursor boven een seat om de naam van de deelnemer te zien.</strong><br /><br />");
	
		if($lanners = $mysqli->query("SELECT nick, catnr FROM plan p LEFT JOIN " . $database['map_visitor_table'] . " d ON p.id = d.catnr ORDER BY nick ASC;"))
		{
			printf("<select>");
			while($lan = $lanners->fetch_object()) {
				if($lan->nick != NULL)
					printf("<option>$lan->nick - $lan->catnr</option>");			
			}
			printf("</select>");
		}
	
	printf("</div>");

    /**
     * Seating Plan
     */
    if ($result = $mysqli->query("SELECT p.id, nick, catnr, aanwezig, x, y FROM plan p LEFT JOIN " . $database['map_visitor_table'] . " d ON p.id = d.catnr")) {
				
		while($seat = $result->fetch_object())
		{
			$yCoord = ($seat->y * 0.8) + 60;
			$xCoord = ($seat->x * 1.1) + 120;
			
			// Status indicator
			$status = "";
			if($seat->aanwezig == 0) $status = "absent";
			if($seat->aanwezig == 1) $status = "taken";
			if($seat->catnr == "") $status = "vacant";

			$nick = empty($seat->nick) ? "OPEN" : $seat->nick;
			
			printf("<div title=\"$nick\" class=\"seat $status\" style=\"right: {$yCoord}px; top: {$xCoord}px;\"><strong>$seat->id</strong></div>");
		}
		
		printf("&nbsp;</div>");
		
	    /* free result set */
	    $result->close();
	}

$mysqli->close();

$x->display_bottom();
?>
