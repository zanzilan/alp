table { border-spacing:0; }

.clear {
    clear: both;
}

.general_box {
    border: 1px solid #303030;
    background: #242424 url(/img/bg-maintitle.png) repeat-x 0% 0%;
    padding: 12px;
    border-radius: 6px;
    -moz-border-radius: 6px;
}

table, form, body { 
	font: 12px Tahoma;
	border: 0px 0px 0px 0px;
	margin: 0px 0px 0px 0px;
	padding: 0px 0px 0px 0px;
}
    
a:hover {
	text-decoration: underline;
	color: <?php echo $colors["primary"]; ?>;
}
    
input, select, textarea, .button {
	font-family: Tahoma;
	font-weight: bold;
	font-size: 12px;
	border: 1px solid <?php echo $colors['border']; ?>;
	margin: 0px 2px 0px 2px;
	padding: 0px 2px 0px 2px;
}

#wrapper {
    width: 980px;
    margin-left: auto;
    margin-right: auto;
    padding: 0;
}    

#header {
    background-color: #202020;
    margin: 10px 5px 0 5px;
    border: 1px solid #202020;
    border-radius: 7px;
    -moz-border-radius: 7px;
    padding: 0;
}

#headerimg {
    border-radius: 7px 7px 0 0;
}

#topbar {
    background: #292929 url('<?php echo $images['headermenu']; ?>') repeat-x;
    height: 30px;
    border-top: 1px solid #202020;
    border-radius: 0 0 7px 7px;
    -moz-border-radius: 0 0 7px 7px;
}

#topmenu {
    float: left;
    margin: 8px 0 5px 10px;
}

#topmenu a:hover {
	border-bottom: 4px solid <?php echo $colors['primary']; ?>;
	color: #fff;
	text-decoration: none;
	padding: 0px 0px 6px 0px;
}

#topcpmenu {
    float: right;
    margin: 7px 10px 0 0;
}

.formcolors {
	background-color: <?php echo $colors['cell_alternate']; ?>;
	color: <?php echo $colors['text']; ?>;
	border-color: <?php echo $colors['border']; ?>;
}
    
.formcolors2 {
	background-color: <?php echo $colors['cell_alternate']; ?>;
	color: <?php echo $colors['text']; ?>;
	border-color: <?php echo $colors['primary']; ?>;
}

.ul {
	border: 1px solid #222222;
	margin: 4px 12px 4px 4px;
	padding: 6px 6px 8px 6px;
}

.normal {
	font: 12px Tahoma;
}
    
.radio {
	border: none;
	text-decoration: none;
}
    
.button {
	font-size: 9px;
	background-color: <?php echo $colors['cell_alternate']; ?>;
}
    
.main {
	cursor: crosshair
}
    
.obj {
	cursor: hand
}
    
.unblend {
	font-weight: bold;
}
    
.menu_top {
	font: 10px Verdana;
}
    
.menu {
	text-decoration: none;
    margin-right: 5px;
}
    
.sm {
	font: 10px Verdana;
}
    
.smm {
	font: 9px Helvetica;
	font-weight: bold;
}
    
.celltitle {
	font: 10px Verdana;
	font-weight: bold;
	background: #78952a;
}
    
.title {
	font: 18px Tahoma;
	font-weight: bold;
}
    
.centerd {
	margin-left: auto;
	margin-right: auto;
}

.tourneytitle {
	font: 24px Arial;
	font-weight: bold;
}

/** Servers */
.srvtbl {
border: 1px #111111 solid;
    -moz-border-radius: 6px;
    border-radius: 6px;
    width: 100%;
    padding: 0px;
}

.srvtbl th {
    padding: 5px;
    background: #111111;
}

.srvtbl td {
    padding: 5px;
}

.srvtbl .sub {
    color: #a9a9a9;
    font-size: 0.8em;
}

.srvtbl .row1 {
    background: #0F0F0F url(/img/row.gif) repeat-x top left; /* Very light blue */
}

.srvtbl .row2 {
    background: #141414 url(/img/row2.gif) repeat-x top left; /* Slightly darker than row1 */
}

    table.ts3_viewer {
      width: 100%;
      border: 0px;
      border-collapse: collapse;
    }

    table.ts3_viewer tr.row1 {
      background: transparent;
    }

    table.ts3_viewer tr.row2 {
      background: #272B25;
    }

    table.ts3_viewer td {
      white-space: nowrap;
      padding: 0px 0px 1px 0px;
      border: 0px;
    }

    table.ts3_viewer td.corpus {
      width: 100%;
    }

    table.ts3_viewer td.query {
      font-style: italic;
      color: #666E73;
    }

    table.ts3_viewer td.spacer {
      overflow: hidden;
    }

    table.ts3_viewer td.left {
      text-align: left;
    }

    table.ts3_viewer td.right {
      text-align: right;
    }

    table.ts3_viewer td.center {
      text-align: center;
    }

    table.ts3_viewer td.suffix {
      vertical-align: top;
    }

    table.ts3_viewer td.suffix img {
      padding-left: 2px;
      vertical-align: top;
    }

    table.ts3_viewer td.spacer.solidline {
      background: url('../../../images/viewer/spacer_solidline.gif') repeat-x;
    }

    table.ts3_viewer td.spacer.dashline {
      background: url('../../../images/viewer/spacer_dashline.gif') repeat-x;
    }

    table.ts3_viewer td.spacer.dashdotline {
      background: url('../../../images/viewer/spacer_dashdotline.gif') repeat-x;
    }

    table.ts3_viewer td.spacer.dashdotdotline {
      background: url('../../../images/viewer/spacer_dashdotdotline.gif') repeat-x;
    }

    table.ts3_viewer td.spacer.dotline {
      background: url('../../../images/viewer/spacer_dotline.gif') repeat-x;
    }

    span.success {
      color: #648434;
    }

    span.error {
      color: #CF3738;
    }
    
    .seatcontainer {
    	position: relative;
    	height: 915px;
    	width: 100%;
    }
    
    .seat {
    	width: 42px;
    	height: 28px;
    	line-height: 28px;
    	text-align: center;
    	
    	color: #000;
    	background: #fff;
    	border: 1px #000 solid;
    	
    	position: absolute;
    	display: block;
    	
    	border-radius: 3px;
    }
    
    .vacant {
    	background: #444;
    	border: 1px #666 solid;
    	color: #000;
    }
    
    .taken {
    	background: #78952a;
    	border: 1px #A2CF29 solid;
    	color: #000;
    }
    
    .absent {
    	background: #802020;
    	border: 1px #A32727 solid;
    	color: #000;
    }
    
    .join_ts {
    	background: #78952a;
    	margin: 5px 5px 15px 5px; 
    	padding: 10px;
    	text-align: center;
    	border: 1px #8EB526 solid;
    	border-radius: 5px;
    }
    
    .lastupdate {
    	font-size: 0.7em;
    	padding-top: 5px;
    	text-align: center;
    }
    
    .notification {
		background: #40A38C;
    	margin: 15px 5px 15px 5px; 
    	padding: 10px;
    	text-align: center;
    	color: #222222;
    	border: 1px #46C2A5 solid;
    	border-radius: 5px;
    }