<div id="wrapper">

    <div id="header">

        <a href="index.php"><img src="img/alp_header_v13z.png" alt="<?php echo $lan["name"]; ?>" id="headerimg" /></a>
        
        <div id="topbar">
        
	        <div id="topmenu">
                <font class="sm">
                <?php if(!ALP_TOURNAMENT_MODE) { ?>
                    <a href="index.php" class="menu" style="color: #ffffff"><strong><?php echo strtoupper(get_lang('home')); ?></strong></a>
                <?php }
                
                if($toggle["files"] && !ALP_TOURNAMENT_MODE) { ?>
                    <a href="files.php" class="menu" style="color: #ffffff"><strong><?php echo strtoupper(get_lang('files')); ?></strong></a><?php
                }
                
                if($toggle["seating"] && !ALP_TOURNAMENT_MODE) { ?>
                    <a href="seating.php" class="menu" style="color: #ffffff"><strong><?php echo strtoupper(get_lang('map')); ?></strong></a><?php
                }
                
                if($toggle["music"] && !ALP_TOURNAMENT_MODE) { ?>
                    <a href="music.php" class="menu" style="color: #ffffff"><strong><?php echo strtoupper(get_lang('music')); ?></strong></a><?php
                }

                if($toggle["schedule"]) { ?>
                    <a href="disp_schedule.php" class="menu" style="color: #ffffff"><strong><?php echo strtoupper(get_lang('schedule')); ?></strong></a><?php
                }
                
                if($toggle["servers"] && ALP_TOURNAMENT_MODE_COMPUTER_GAMES) { ?>
                    <a href="servers.php" class="menu" style="color: #ffffff"><strong><?php echo strtoupper(get_lang('servers')); ?></strong></a><?php
                } ?>
                
                <a href="tournaments.php" class="menu" style="color: #ffffff"><strong><?php echo strtoupper(get_lang('tournaments')); ?></strong></a>
                
                <?php
                    if($toggle["sponsors"] && !ALP_TOURNAMENT_MODE) { ?>
                        <a href="disp_sponsors.php" class="menu" style="color: #ffffff"><strong><?php echo strtoupper(get_lang('sponsors')); ?></strong></a><?php
                    }
                    
                    if(!ALP_TOURNAMENT_MODE && $toggle["staff"]) { ?>
                        <a href="staff.php" class="menu" style="color: #ffffff"><strong><?php echo strtoupper(get_lang('staff')); ?></strong></a><?php
                    }
                    
                    if(!ALP_TOURNAMENT_MODE) { ?>
                        <a href="users.php" class="menu" style="color: #ffffff"><strong><?php echo strtoupper(get_lang('users')); ?></strong></a></font><?php
                    }
                ?>
            </div>
            
	        <div id="topcpmenu">
                <?php 
		            echo cp_menu(); //include/_functions.php
                ?>
	        </div>
            
            <div class="clear">&nbsp;</div>
            
        </div>
    </div>

    <table border="0" cellpadding="0" cellspacing="0" width="980" bgcolor="<?php echo $colors["background"]; ?>">
    <tr><td>
	    <?php spacer(1,$container['verticalpadding']); ?>
	    <table border="0" cellpadding="0" cellspacing="0" width="100%">
		    <tr>
			    <td width="<?php $modules->get_Width("left"); ?>" valign="top"><?php $modules->display_all_modules("left"); ?></td>
			    <td width="100%" valign="top">
